Legacy TE Insta-launcher by PyotrLuzhin and Yohan1044

This boot.elf launcher will skip the launcher screen and boot straight to the game.
The launcher does not contain SDHC support, and can only be used with 2GB SD cards.
Replace the boot.elf on the root of the SD card (for hackless) or in the apps folder (for homebrew).